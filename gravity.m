clear all
points = 100*rand(250,2);
klasy=num2cell(linspace(1,length(points),length(points)));
old=points
m=ones(length(points),1);
bufor = zeros(length(points),2);
plot(points(:,1),points(:,2), '*')
figure
    meaning=mean(points);
    rozmiar=length(points);
time=0;
h=0;
g=0;
for k=1:100000
    %max(points)
if(length(points)<5)
    break
end
    dt=1000000;
P=zeros(length(points),2);
S=zeros(length(points),2);
G=zeros(length(points),length(points),2);
grupy=num2cell(points,2);
for i=1:length(grupy)
    grupy{i,2}=grupy{i,1};
end
for i=1:length(points)
    %S(i,k,:)=[0 0];
    S(i,:)=((rozmiar/(1+time)))*(points(i,:)-meaning)/(norm(points(i,:)-meaning));
    for j=i:length(points)
        
        if j~=i
            odl = (norm(points(i,:)-points(j,:)));
            if (odl<1)
               odl=1; 
            end
         G(i,j,:)=-(m(j)*m(i))*(points(i,:)-points(j,:))/(odl^2);
         
                  G(j,i,:)=-G(i,j,:);
         if tanh((norm(grupy{i,2}-points(j,:)))^(1/(norm(grupy{i,2}-points(j,:))))) < dt
                         grupy{i}=[grupy{i}; grupy{j}];

             dt = tanh((norm(points(i,:)-points(j,:)))^(1/(norm(points(i,:)-points(j,:)))));
             h=i;
             g=j;
         end
        end
    end

        SumG=[sum(G(i,:,1)) sum(G(i,:,2))];
        SumS=[S(i,1) S(i,2)];
        P(i,:) = dt*(SumS+SumG)/m(i);
    bufor(i,:)=[P(i,1) P(i,2)]+points(i,:);
end
dt;
time=time+dt
length(points)
bufer=bufor(1:h,:);
 if dt < 0.7
     f=h;
for i=h:length(points)
    
        
       
        if (i==h)
        
            bufer(f,:)=(m(i)*bufor(i,:)+m(g)*bufor(g,:))/(m(i)+m(g));
            m(f)=m(i)+m(g);
            klasy{f}=[klasy{f}; klasy{g}];
        f=f+1;    
            
        
        elseif (i~=g)
                        bufer(f,:)=bufor(i,:);
            m(f)=m(i);
            klasy{f}=klasy{i};
                        f=f+1;
        end
    end
 bufor=bufer;
 klas=klasy(1:f-1);
 klasy=klas;
 mass=m(1:f-1);
 m=mass;

 end
for i=1:length(grupy)
    grupy{i,2}=[(mean(grupy{i}))];
end
 plot(bufor(:,1),bufor(:,2), '*')

pause(0.001)
points=bufor;
end
figure
for k=1:length(klasy)
    H=[];
    for j=1:length(klasy{k})
   H(j,:)=old(klasy{k}(j),:);     
    end
    plot(H(:,1),H(:,2),'*')
    hold on
end
hold off